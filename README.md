# Transaction List
A mobile application built using React Native

### Installation
Install the dependencies and devDependencies and start the server.

```sh
#clone the repo
git clone https://gitlab.com/oman21/transaction-list.git
cd edutech-mobile

# Install npm dependencies
yarn
#or
npm install
```