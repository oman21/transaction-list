/**
 * @author Oman Fathurohman <oman21.dev@gmail.com>
 */

import React from 'react';
import { NavigationContainer } from '@react-navigation/native';;
import { createStackNavigator } from '@react-navigation/stack';
import TransactionStack from './src/routes/TransactionStack';
import { Provider as StoreProvider } from 'react-redux'
import store from './src/redux/store'

const Stack = createStackNavigator();

const App = () => {
  
  return(
    <StoreProvider store={store}>
      <NavigationContainer>
        <Stack.Navigator
        initialRouteName={TransactionStack}
        screenOptions={{
          headerShown: false
        }}>
          <Stack.Screen
            name="TransactionStack"
            component={TransactionStack}/>
        </Stack.Navigator>
      </NavigationContainer>
    </StoreProvider>
  )
}

export default App
