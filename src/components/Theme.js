import React, { useState } from 'react';
import { Text, View } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { SuccessColor, DangerColor } from "./Color";
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { ThousanSeparator, DateFormat } from './GlobalFunction'

export const NunitoText = (props) => {
	return (
		<Text {...props} style={[{fontFamily: 'Nunito-Regular'}, props.style]}>{props.children}</Text>
	)
}

export const NunitoTextBold = (props) => {
	return (
		<Text {...props} style={[{fontFamily: 'Nunito-ExtraBold'}, props.style]}>{props.children}</Text>
	)
}

export const Card = (props) => {
	var colorBorder, colorBg, colorText;

	if(props.status=='SUCCESS') {
		colorBorder = SuccessColor();
		colorBg = SuccessColor();
		colorText = '#fff';
	}else {
		colorBorder = DangerColor();
		colorBg = '#fff';
		colorText = '#000';
	};

	return(
		<View style={{backgroundColor: 'white', borderRadius: 10, overflow: 'hidden'}}>
			<View style={{borderLeftWidth: 8, borderLeftColor: colorBorder, padding: 20, flexDirection: 'row', justifyContent:'center', alignItems: 'center'}}>
				<View style={{flex:1}}>
					<NunitoTextBold style={{ fontSize: 18, textTransform: 'uppercase' }}>{props.senderBank} ➜ {props.beneficiaryBank}</NunitoTextBold>
					<NunitoText style={{textTransform: 'uppercase'}}>{props.beneficiaryName}</NunitoText>
					<NunitoText>Rp{ThousanSeparator(props.amount)} • {DateFormat(props.date)}</NunitoText>
				</View>
				<View style={{flex:0}}>
					<View style={{borderRadius: 6, borderWidth: 2, borderColor: colorBorder, paddingHorizontal: 10, paddingVertical:3, backgroundColor: colorBg}}>
						<NunitoTextBold style={{ textTransform: 'capitalize', color: colorText }}>{props.status}</NunitoTextBold>
					</View>
				</View>
			</View>
		</View>
	)
}

export const SortModal = (props) => {
	const [active, setActive] = useState('FILTER_INACTIVE');
	const list = [
		{ value: 'FILTER_INACTIVE', name: "URUTKAN" },
		{ value: 'FILTER_NAME_A_Z', name: "Nama A-Z" },
		{ value: 'FILTER_NAME_Z_A', name: "Nama Z-A" },
		{ value: 'FILTER_DATE_A_Z', name: "Tanggal Terbaru" },
		{ value: 'FILTER_DATE_Z_A', name: "Tanggal Terlama" }
	];

	if(props.show){
		return(
			<View style={{backgroundColor:'#000000a6', position:'absolute', top:0, bottom:0, left:0, right:0, zIndex: 2, flex: 1, justifyContent: 'center', alignItems: 'center'}}>
				<View style={{backgroundColor: 'white', borderRadius: 6, paddingHorizontal:15, paddingVertical: 20, minWidth: 220}}>
					{
						list.map((item, index)=>{
							return(
								<TouchableOpacity style={{flexDirection: 'row', alignItems:'center', marginTop:index==0?0:15}} key={'list-'+index.toString()} onPress={()=>onChange(item)}>
									<MaterialCommunityIcons name={active==item.value?'circle-slice-8':'circle-outline'} size={22} color={DangerColor()}/>
									<NunitoText style={{paddingLeft:10, fontSize:16}}>{item.name}</NunitoText>
								</TouchableOpacity>
							)
						})
					}
				</View>

			</View>
		)
	}else{
		return <View/>
	}

	function onChange(item){
		setActive(item.value)
		props.onChangeSort(item);
  }
}