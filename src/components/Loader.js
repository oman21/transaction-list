import React from 'react'
import { View, ActivityIndicator } from 'react-native'
import { BrandColor } from './Color';
import { NunitoTextBold } from './Theme';

export const Loader = (props) => {
	if(props.status){
		return(
			<View style={{backgroundColor:'#000000a6', position:'absolute', top:0, bottom:0, left:0, right:0, zIndex: 2, flex: 1, justifyContent: 'center', alignItems: 'center'}}>
				<View style={{backgroundColor: 'white', borderRadius: 6, paddingHorizontal:15, paddingVertical: 20}}>
					<ActivityIndicator size="large" color={BrandColor()} />
					<NunitoTextBold>Memuat Data</NunitoTextBold>
				</View>
			</View>
		)
	}

	return <View/>
}