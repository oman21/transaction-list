const brandColor = '#098cf6';
const successColor = '#55b684';
const dangerColor = '#f56840';

export const BrandColor = () =>{
	return brandColor;
}

export const SuccessColor = () =>{
	return successColor;
}

export const DangerColor = () =>{
	return dangerColor;
}