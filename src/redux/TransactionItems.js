export const ADD_CURRENTLY_DATA = 'ADD_CURRENTLY_DATA'

const initialState = []

const transactionItemsReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_CURRENTLY_DATA:
      return action.payload
  }
  return state
}

export default transactionItemsReducer