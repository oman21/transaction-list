import { createStore } from 'redux'
import transactionItemsReducer from './TransactionItems'

const store = createStore(transactionItemsReducer)

export default store