import React from 'react';
import { SafeAreaView, View, TouchableOpacity, ToastAndroid } from 'react-native';
import { NunitoTextBold, NunitoText } from './../components/Theme';
import { DangerColor } from './../components/Color';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { DateFormat, ThousanSeparator } from './../components/GlobalFunction';
import Clipboard from '@react-native-community/clipboard';

const TransactionDetail = ({ route, navigation }) => {

	const data = route.params?.data;
  
  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>

			<View style={{padding: 20, borderBottomColor: '#eee', borderBottomWidth: 1, flexDirection:'row', alignItems: 'center'}}>
				<NunitoTextBold>ID TRANSAKSI: #{data.id}</NunitoTextBold>
				<TouchableOpacity style={{marginLeft:10}} onPress={()=>copy()}>
					<MaterialCommunityIcons name={'content-copy'} color={DangerColor()} size={20}/>
				</TouchableOpacity>
			</View>
			<View style={{padding: 20, borderBottomColor: '#ccc', borderBottomWidth: 1, flexDirection:'row', alignItems: 'center'}}>
				<NunitoTextBold style={{flex:1}}>DETAIL TRANSAKSI</NunitoTextBold>
				<TouchableOpacity onPress={()=>navigation.goBack()}>
					<NunitoText style={{color: DangerColor()}}>Tutup</NunitoText>
				</TouchableOpacity>
			</View>
			<View style={{padding:20}}>
				<NunitoTextBold style={{fontSize:18, textTransform: 'uppercase'}}>{data.sender_bank} ➜ {data.beneficiary_bank}</NunitoTextBold>

				<View style={{flexDirection:'row', marginTop: 15}}>
					<View style={{flex:3}}>
						<NunitoTextBold>{data.beneficiary_name}</NunitoTextBold>
						<NunitoText>{data.account_number}</NunitoText>
					</View>
					<View style={{flex:2}}>
						<NunitoTextBold>NOMINAL</NunitoTextBold>
						<NunitoText>Rp{ThousanSeparator(data.amount)}</NunitoText>
					</View>
				</View>

				<View style={{flexDirection:'row', marginTop: 15}}>
					<View style={{flex:3}}>
						<NunitoTextBold>CERITA TRANSFER</NunitoTextBold>
						<NunitoText>{data.remark}</NunitoText>
					</View>
					<View style={{flex:2}}>
						<NunitoTextBold>KODE</NunitoTextBold>
						<NunitoText>{data.unique_code}</NunitoText>
					</View>
				</View>

				<View style={{marginTop: 15}}>
					<NunitoTextBold>WAKTU TRANSFER</NunitoTextBold>
					<NunitoText>{DateFormat(data.completed_at)}</NunitoText>
				</View>
			</View>
    </SafeAreaView>
	);
	
	function copy(){
		Clipboard.setString(data.id);
		ToastAndroid.show("Copied to clipboard", ToastAndroid.SHORT);
	}
}

export default TransactionDetail;