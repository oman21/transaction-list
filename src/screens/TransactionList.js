import React, { useState, useEffect } from 'react'
import { SafeAreaView, View, Alert, FlatList, TextInput, TouchableOpacity, RefreshControl, BackHandler } from 'react-native'
import { NunitoTextBold, Card, SortModal, NunitoText } from './../components/Theme'
import { DangerColor } from './../components/Color'
import EvilIcons from 'react-native-vector-icons/EvilIcons'
import Entypo from 'react-native-vector-icons/Entypo'
import { useDispatch } from 'react-redux'
import { ADD_CURRENTLY_DATA } from '../redux/TransactionItems'
import { useSelector } from 'react-redux'

const TransactionList = ({ navigation }) => {

	const currentlyData = useSelector(state => state);
	const [data, setData] = useState([]);
	const [loading, setLoading] = useState(true);
	const [search, setSearch] = useState("");
	const [sortModal, setSortModal] = useState(false);
	const [sortName, setSortName] = useState('URUTKAN');
	const [sortValue, setSortValue] = useState('FILTER_INACTIVE');
	const [fetchIndicator, setFetchIndicator] = useState(false);

	const dispatch = useDispatch()

	const filter = () => {
		// sorting
		if(sortValue=='FILTER_NAME_A_Z') { // mengurutkan nama dari a-z
			var new_data = currentlyData.sort((a, b) => (a.beneficiary_name < b.beneficiary_name ? -1 : 1));
		}else if(sortValue=='FILTER_NAME_Z_A') { // mengurutkan nama dari z-a
			var new_data = currentlyData.sort((a, b) => (a.beneficiary_name > b.beneficiary_name ? -1 : 1));
		}else if(sortValue=='FILTER_DATE_A_Z') { // mengurutkan tanggal terbaru
			var new_data = currentlyData.sort((a, b) => {
				var dateA = new Date(a.completed_at), dateB = new Date(b.completed_at);
				return dateA - dateB;
			});
		}else if(sortValue=='FILTER_DATE_Z_A') { // mengurutkan tanggal terlama
			var new_data = currentlyData.sort((a, b) => {
				var dateA = new Date(a.completed_at), dateB = new Date(b.completed_at);
				return dateB - dateA;
			});
		}else{
			var new_data = currentlyData;
		}
		
		// searching from sorting
		var result = [];
		for(var i=0; i<new_data.length; i++){
			if(
				new_data[i].sender_bank.toString().toLowerCase().indexOf(search.toString().toLowerCase()) > -1 ||
				new_data[i].beneficiary_bank.toString().toLowerCase().indexOf(search.toString().toLowerCase()) > -1 ||
				new_data[i].beneficiary_name.toString().toLowerCase().indexOf(search.toString().toLowerCase()) > -1 ||
				new_data[i].amount.toString().toLowerCase().indexOf(search.toString().toLowerCase()) > -1
			){
				result.push(new_data[i])
			}
		}
		setData(result)
		setLoading(false)
	}

	const getListTransaction = async () => {
		setLoading(true);
		try {
			const apiCall = await fetch('https://nextar.flip.id/frontend-test');
			const api = await apiCall.json();
			var result = Object.keys(api).map((key) => api[key]);
			dispatch({ type: ADD_CURRENTLY_DATA, payload: result });
			setFetchIndicator(true); // untuk mendeteksi jika data berhasil di dapat maka langsung di sorting dan search yang aktif
		} catch(err) {
			setLoading(false);
			Alert.alert(
				"Error",
				"Terjadi Kesalahan Saat Memuat Data",
				[
					{ text: "OK" }
				],
				{ cancelable: false }
			);
		}
	}

	useEffect(()=>{
		getListTransaction(); // menjalankan funtion untuk mendapatkan data dari api
	},[])

	// handle back button jika modal sorting aktif
	useEffect(()=>{
		const backAction = () => {
			if(sortModal){
				setSortModal(false);
				return true;
			}

			return false;
		};

		const backHandler = BackHandler.addEventListener(
			"hardwareBackPress",
			backAction
		);

		return () => backHandler.remove();
	}, [sortModal])

	useEffect(()=> {
		filter(); // menjalankan function filter setiap ada perubahan searching bar dan sorting
		setFetchIndicator(false) // ubah lagi supaya bisa nanti pas pull referesh sorting dan search langsung aktif
	},[sortValue, search, fetchIndicator])
  
  return (
    <SafeAreaView style={{ flex: 1 }}>
			<SortModal show={sortModal} onChangeSort={(value) => sorting(value)}/>

			<View style={{ padding: 10, flex: 1}}>
				{/* search and srting bar */}
				<View style={{backgroundColor: 'white', padding: 10, marginBottom: 10, flexDirection: 'row', alignItems: 'center', elevation: 3}}>
					<EvilIcons name={'search'} size={22}/>
					<TextInput
						style={{ flex: 1, padding: 0 , marginLeft: 6}}
						placeholder={'Cari nama, bank, atau nominal'}
						value={search}
						onChangeText={text => setSearch(text)}
					/>
					<TouchableOpacity style={{flexDirection: 'row', alignItems: 'center'}} onPress={()=>setSortModal(true)}>
						<NunitoTextBold style={{ color: DangerColor() }}>{sortName}</NunitoTextBold>
						<Entypo name={'chevron-small-down'} size={22} color={DangerColor()}/>
					</TouchableOpacity>
				</View>
				{/* end search and srting bar */}

				{
					// jika data tidak ada
					data.length==0 && search.length>0?<NunitoText style={{textAlign:'center', marginTop:20}}>Transaksi Tidak Ditemukan</NunitoText>:<View/>
				}

				{
					// jika loading aktif
					loading?<NunitoText style={{textAlign:'center', marginVertical:10}}>Memuat Data</NunitoText>:<View/>
				}

				<FlatList
					data={data}
					renderItem={({item})=>{
						return(
							<TouchableOpacity onPress={() => navigation.navigate('TransactionDetail', { data: item })}>
								<Card
									senderBank={item.sender_bank}
									beneficiaryBank={item.beneficiary_bank}
									beneficiaryName={item.beneficiary_name}
									amount={item.amount}
									date={item.completed_at}
									status={item.status}
								/>
							</TouchableOpacity>
						)
					}}
					keyExtractor={item => item.id}
					showsVerticalScrollIndicator={false}
					refreshControl={
						<RefreshControl
							refreshing={loading}
							 onRefresh={()=>getListTransaction()}
						/>
					}
					ItemSeparatorComponent={()=><View style={{height:7}}/>}
				/>	
			</View>
    </SafeAreaView>
	)

	function sorting(item) {
		setSortModal(false);
		setSortName(item.name);
		setSortValue(item.value);
	}
}

export default TransactionList;