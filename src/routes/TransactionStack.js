import * as React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import TransactionList from './../screens/TransactionList';
import TransactionDetail from '../screens/TransactionDetail';

const Stack = createStackNavigator();
Stack.navigationOptions = ({ navigation }) => {
  let tabBarVisible = true;
  if (navigation.state.index > 0) {
    tabBarVisible = false;
  }

  return {
    tabBarVisible,
  };
};

function TransactionStack() {
  return (
    <Stack.Navigator
      initialRouteName="TransactionList"
      screenOptions={{
        headerShown: false
      }}>
      <Stack.Screen
        name="TransactionList"
        component={TransactionList}/>
			<Stack.Screen
        name="TransactionDetail"
        component={TransactionDetail}/>
    </Stack.Navigator>
  );
}

export default TransactionStack;